require 'byebug'
class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @game = ""
  end

  def play
    setup
    take_turn until over?
    conclude?
  end

  def setup
   length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess= guesser.guess(@board)
    indices = referee.check_guess(guess)
    update_board(indices,guess)
    @guesser.handle_response(guess,indices)

  end

  def update_board(indices,letter)
    indices.each { |idx| @board[idx] = letter }
  end


  def handle_response

  end

  def over?
    @board.count(nil) == 0
  end

  def conclude?
    p "Congrats"
  end



end

class HumanPlayer
  def initialize
    @guessed_letters = []
  end

  def handle_response(guess,indices)
  end

  def register_secret_length(length)
    p "The Secret Word has length: #{length}"
  end

  def guess(board)
    print_board(board)
    p ">"
    guess = gets.chomp.downcase
    process_guess(guess)
  end

  def pick_secret_word
    print "How long is the word you would like to use?"
    gets.chomp.to_i
  end

  def check_guess(letter)
    puts "The computer has guessed the letter #{letter}"
    print "The indices of that letter are "
     gets.chomp.split.map { |el| el.to_i }
  end

  private

  def print_board(board)
    board_string = board.map do |el|
      el.nil? ? "_" : el
    end.join("")
    puts "Secret Word: #{board_string}"
  end

  def process_guess (guess)
    unless @guessed_letters.include?(guess)
      @guessed_letters << guess
      return guess
    else
      puts "You have already guessed #{guess}"
      puts "You have guess the following:"
      p @guessed_letters
      puts "Please Guess again"
      puts ">"
      guess = gets.chomp
      process_guess(guess)
    end
  end


class ComputerPlayer
  attr_reader :dictionary_word

  def self.read_dictionary
    File.readlines('lib/dictionary.txt').map(&:chomp)
  end

  def initialize(dictionary)
    @dictionary = dictionary
    @alphabet = ('a'..'z').to_a
  end

  def candidate_words
    @dictionary
  end

  def pick_secret_word
  @secret_word =  @dictionary.sample
  @secret_word.length
  end

  def check_guess(letter)
    ans = []
    @secret_word.chars.each_with_index do |let,idx|
      if let == letter
        ans << idx
      end
    end
    ans
  end

  def register_secret_length(length)
    # @known_letters = Array.new(length)
    @secret_length = length
    @dictionary.select! {|word| word.length == length}
  end

  def guess (board)
    best = @alphabet.first
    best_count = 0
    @alphabet.each do |letter|
      count = @dictionary.count { |word| word.include?(letter)}
      if count > best_count
        best = letter
        best_count = count
      end
    end
    @alphabet.delete(best)
    best
  end

  def handle_response(letter,indices)
    @dictionary.select! do |word|
      word_indices = []
      word.chars.each_with_index do |char,idx|
        word_indices << idx if char == letter
      end
      indices == word_indices
      # indices.all? { |idx| word[idx] == letter }
    end
  end






end

if __FILE__ == $PROGRAM_NAME
  dictionary = ComputerPlayer.read_dictionary
  players = {
    referee: HumanPlayer.new,
    guesser: ComputerPlayer.new(dictionary)
  }
  game = Hangman.new(players)
  game.play
end

end
